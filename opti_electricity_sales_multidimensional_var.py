"""
 Pyomo documention:
 https://pyomo.readthedocs.io/en/stable/
 Pyomo tutorial:
 https://www.osti.gov/servlets/purl/1376827
 Pyomo examples:
 https://github.com/Pyomo/pyomo
"""

from pyomo.environ import *

# This linear optimization problem (LP) asks for the right market to sell
# electricity to maximize to profit

""" Inputs """
# Electricity production
production = [2.1, 3.2, 1.8, 0.5, 1.2]

prices = {'Market_A': [12, 0.8, 14, 3.6, 19], 'Market_B': [20, 13, 5, 6, 10]}
limitations = {'Market_A': [1.9, 5, 1.5, 2, 0.6], 'Market_B': [1.9, 5, 1.5, 2, 0.6]}

""" Start modelling """
# Creating a concrete model. (abstract models also possible)
model = ConcreteModel(name="Electricity Sales")

""" Sets """
# Defines a Set. A set is an objects for managing indices for variables.
# It helps to iterable through variables
# Multidimensional indices are also possible, as you can see in the following
# model.timeSteps = RangeSet(0, len(production) - 1)
model.timeSteps = Set(initialize=range(len(production)))
model.markets = ['Market_A', 'Market_B']

""" Variables """
model.market_sales = Var(model.markets, model.timeSteps, within=NonNegativeReals)


""" Rules """
def energy_balance(model, timestep):
    return sum(model.market_sales[market, timestep] for market in model.markets) == \
           production[timestep]


def market_limit(model, market, timestep):
    return model.market_sales[market, timestep] <= limitations[market][timestep]


def objective(model):
    return sum(model.market_sales[market, timestep] * prices[market][timestep]
                   for market in model.markets for timestep in model.timeSteps)


""" Constraints """
model.energy_balance = Constraint(model.timeSteps, rule=energy_balance)
model.market_limit = Constraint(model.markets, model.timeSteps, rule=market_limit)

""" Objective """
model.profit = Objective(sense=maximize, rule=objective)

""" Solving and output """
opt = SolverFactory('cbc')
# .solve function calls the solver hands over the defined model.
# the solver will solve the model and hands over the results.
# tee=True prints solver information
# logfile saves the solver printout in the a file by the given name e.g. 'log_file'
model.write('pyomo_electricity_sales.lp')
result_obj = opt.solve(model, tee=True, logfile='log_file_electricity_sales')

# prints model
model.pprint()

# Prints calculated values for varaible 'sales_market_a' and 'sales_market_b'
for timestep in model.timeSteps:
    print('Timestep', timestep, 'Production', round(production[timestep], 2))
    sum_sales = 0
    for market in model.markets:
        value_sales = model.market_sales[market, timestep].value
        sum_sales += value_sales
        print('  Sales', market, round(value_sales, 2))
    print('  Sum Sales', sum_sales)

print('\nProfit', model.profit())
