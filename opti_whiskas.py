"""
 Pyomo documention:
 https://pyomo.readthedocs.io/en/stable/
 Pyomo tutorial:
 https://www.osti.gov/servlets/purl/1376827
 Pyomo examples:
 https://github.com/Pyomo/pyomo
"""

from pyomo.environ import *

# This linear optimization problem (LP) asks for the right ingredients
# for cat food. The costs should be minimize while constraints consider
# several nutrition values that need to be reached

# Creates a list of the Ingredients
Ingredients = ['CHICKEN', 'BEEF', 'MUTTON', 'RICE', 'WHEAT', 'GEL']

# A dictionary of the costs of each of the Ingredients is created
costs = {'CHICKEN': 0.013,
         'BEEF': 0.008,
         'MUTTON': 0.010,
         'RICE': 0.002,
         'WHEAT': 0.005,
         'GEL': 0.001}

# A dictionary of the protein percent in each of the Ingredients is created
proteinPercent = {'CHICKEN': 0.100,
                  'BEEF': 0.200,
                  'MUTTON': 0.150,
                  'RICE': 0.000,
                  'WHEAT': 0.040,
                  'GEL': 0.000}

# A dictionary of the fat percent in each of the Ingredients is created
fatPercent = {'CHICKEN': 0.080,
              'BEEF': 0.100,
              'MUTTON': 0.110,
              'RICE': 0.010,
              'WHEAT': 0.010,
              'GEL': 0.000}

# A dictionary of the fibre percent in each of the Ingredients is created
fibrePercent = {'CHICKEN': 0.001,
                'BEEF': 0.005,
                'MUTTON': 0.003,
                'RICE': 0.100,
                'WHEAT': 0.150,
                'GEL': 0.000}

# A dictionary of the salt percent in each of the Ingredients is created
saltPercent = {'CHICKEN': 0.002,
               'BEEF': 0.005,
               'MUTTON': 0.007,
               'RICE': 0.002,
               'WHEAT': 0.008,
               'GEL': 0.000}

# Creating a concrete model. (abstract models also possible)
model = ConcreteModel(name="The Whiskas Problem")

# Creating Variables ingredient_vars for every item in the list of Ingredients.
# bounds defines an interval for the variables ingredient_vars
model.ingredient_vars = Var(Ingredients, bounds=(0,None), doc="The amount of each ingredient that is used")

# Defines the objective of the LP. expr defines the expression of the objective
# If no sense is given the default is minimize
model.obj = Objective(expr=sum(costs[i]*model.ingredient_vars[i] for i in Ingredients), doc="Total Cost of Ingredients per can")

# Constraints are defined. expr defines the expressions of the constraints
model.c0 = Constraint(expr=sum(model.ingredient_vars[i] for i in Ingredients) == 100, doc="PercentagesSum")
model.c1 = Constraint(expr=sum(proteinPercent[i] * model.ingredient_vars[i] for i in Ingredients) >= 8.0, doc="ProteinRequirement")
model.c2 = Constraint(expr=sum(fatPercent[i] * model.ingredient_vars[i] for i in Ingredients) >= 6.0, doc="FatRequirement")
model.c3 = Constraint(expr=sum(fibrePercent[i] * model.ingredient_vars[i] for i in Ingredients) <= 2.0, doc="FibreRequirement")
model.c4 = Constraint(expr=sum(saltPercent[i] * model.ingredient_vars[i] for i in Ingredients) <= 0.4, doc="SaltRequirement")

# Pyomo just defines the system of equation but can't solve it.
# To solve the system of equation you need to define a solver in SolverFactory. e.g. cplex
opt = SolverFactory('cplex')

# .solve function calls the solver (e.g. cplex) hands over the defined model.
# the solver will solve the model and hands over the results.
# tee=True prints solver information
# logfile saves the solver printout in the a file by the given name e.g. 'log_file'
model.write('pyomo_whiskas.lp')
result_obj = opt.solve(model, tee=True, logfile='log_file')

# prints model
model.pprint()


# Prints each ingredient and its value per can
for i in model.ingredient_vars:
    ingredient = i
    value = model.ingredient_vars[i].value
    cost = float(costs[i]) * float(value)
    print(ingredient, value, 'costs:', cost)


# Prints the objective (in this case the minimized costs)
print(model.obj())
