"""
 Pyomo documention:
 https://pyomo.readthedocs.io/en/stable/
 Pyomo tutorial:
 https://www.osti.gov/servlets/purl/1376827
 Pyomo examples:
 https://github.com/Pyomo/pyomo
"""

from pyomo.environ import *

# This linear optimization problem (LP) asks for the right market to sell
# electricity to maximize to profit

""" Inputs """
# Electricity production
production = [2.1, 3.2, 1.8, 0.5, 1.2]

# Prices and limitaion of market A
prices_market_a = [12, 0.8, 14, 3.6, 19]
limitation_market_a = [1.9, 5, 1.5, 2, 0.6]

# Prices and limitaion of market B
prices_market_b = [20, 13, 5, 6, 10]
limitation_market_b = [1.9, 5, 1.5, 2, 0.6]

""" Start modelling """
# Creating a concrete model. (abstract models also possible)
model = ConcreteModel(name="Electricity Sales")

""" Sets """
# Defines a Set. A set is an objects for managing indices for variables.
# It helps to iterable through variables
# Multidimensional indices are also possible
model.timeSteps = RangeSet(0, len(production) - 1)

""" Variables """
model.sales_market_a = Var(model.timeSteps, within=NonNegativeReals)
model.sales_market_b = Var(model.timeSteps, within=NonNegativeReals)

""" Rules """
def energy_balance(model, timestep):
    return model.sales_market_a[timestep] + model.sales_market_b[timestep] == \
           production[timestep]


def market_a_limit(model, timestep):
    return model.sales_market_a[timestep] <= limitation_market_a[timestep]


def market_b_limit(model, timestep):
    return model.sales_market_b[timestep] <= limitation_market_b[timestep]


def objective(model):
    return sum(model.sales_market_a[timesteps] * prices_market_a[timesteps] +
                               model.sales_market_b[timesteps] * prices_market_b[timesteps]
                               for timesteps in model.timeSteps)


""" Constraints """
model.energy_balance = Constraint(model.timeSteps, rule=energy_balance)
model.market_a_limit = Constraint(model.timeSteps, rule=market_a_limit)
model.market_b_limit = Constraint(model.timeSteps, rule=market_b_limit)

""" Objective """
model.profit = Objective(sense=maximize, rule=objective)

""" Solving and output """
opt = SolverFactory('cbc')
# .solve function calls the solver hands over the defined model.
# the solver will solve the model and hands over the results.
# tee=True prints solver information
# logfile saves the solver printout in the a file by the given name e.g. 'log_file'
model.write('pyomo_electricity_sales.lp')
result_obj = opt.solve(model, tee=True, logfile='log_file_electricity_sales')

# prints model
model.pprint()

# Prints calculated values for varaible 'sales_market_a' and 'sales_market_b'
for timestep in model.timeSteps:
    value_sales_market_a = model.sales_market_a[timestep].value
    value_sales_market_b = model.sales_market_b[timestep].value
    print('Timestep', timestep, 'Production', round(production[timestep], 2),
          'Sales Market A', round(value_sales_market_a, 2), 'Sales Market B', round(value_sales_market_b, 2))

print('Profit', model.profit())
