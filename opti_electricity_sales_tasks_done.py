"""
 Pyomo documention:
 https://pyomo.readthedocs.io/en/stable/
 Pyomo tutorial:
 https://www.osti.gov/servlets/purl/1376827
 Pyomo examples:
 https://github.com/Pyomo/pyomo
"""

from pyomo.environ import *

# This linear optimization problem (LP) asks for the right market to sell
# electricity to maximize to profit

""" Inputs """
# Electricity production
production = [2.1, 3.2, 1.8, 0.5, 1.3]

# Prices and limitaion of market A
prices_market_a = [12, 0.8, 14, 3.6, 19]
limitaion_market_a = [1.9, 5, 1.5, 2, 0.6]

# Prices and limitaion of market B
prices_market_b = [20, 13, 5, 6, 10]
limitaion_market_b = [1.9, 5, 1.5, 2, 0.6]

""" Start modelling """
# Creating a concrete model. (abstract models also possible)
model = ConcreteModel(name="Electricity Sales")

""" Sets """
# Defines a Set.A Set is an objects for managing multidimensional indices.
# It helps to iterable through variables
model.timeSteps = RangeSet(0, len(production) - 1)

""" Variables """
model.sales_market_a = Var(model.timeSteps, within=NonNegativeReals)
model.sales_market_b = Var(model.timeSteps, within=NonNegativeReals)
model.sum_profit_market_a = Var(within=Reals)
model.sum_profit_market_b = Var(within=Reals)

""" Rules """
def energy_balance(model, timestep):
    return model.sales_market_a[timestep] + model.sales_market_b[timestep] <= \
           production[timestep]


def market_a_limit(model, timestep):
    return model.sales_market_a[timestep] <= limitaion_market_a[timestep]


def market_b_limit(model, timestep):
    return model.sales_market_b[timestep] <= limitaion_market_b[timestep]


def sum_profit_market_a(model):
    return model.sum_profit_market_a == sum(model.sales_market_a[timestep] * prices_market_a[timestep]
                                            for timestep in model.timeSteps)


def sum_profit_market_b(model):
    return model.sum_profit_market_b == sum(model.sales_market_b[timestep] * prices_market_b[timestep]
                                            for timestep in model.timeSteps)


def objective(model):
    return model.sum_profit_market_a + model.sum_profit_market_b


""" Constraints """
model.energy_balance = Constraint(model.timeSteps, rule=energy_balance)
model.market_a_limit = Constraint(model.timeSteps, rule=market_a_limit)
model.market_b_limit = Constraint(model.timeSteps, rule=market_b_limit)
model.c_sum_profit_market_a = Constraint(rule=sum_profit_market_a)
model.c_sum_profit_market_b = Constraint(rule=sum_profit_market_b)

""" Objective """
model.profit = Objective(sense=maximize, rule=objective)


""" Solving and output """
opt = SolverFactory('cplex')
# .solve function calls the solver (e.g. cplex) hands over the defined model.
# the solver will solve the model and hands over the results.
# tee=True prints solver information
# logfile saves the solver printout in the a file by the given name e.g. 'log_file'
model.write('pyomo_electricity.lp')
result_obj = opt.solve(model, tee=True)

# prints model
model.pprint()

# Prints calculated values for varaible 'sales_market_a' and 'sales_market_b'
for i in range(len(model.timeSteps) - 1):
    value_sales_market_a = model.sales_market_a[i].value
    value_sales_market_b = model.sales_market_b[i].value
    print('Timestep', i, 'Production', round(production[i], 2),
          'Sales Market A', round(value_sales_market_a, 2), 'Sales Market B', round(value_sales_market_b, 2))

print('Profit', model.profit())

""" Notice that the Deterministic time increased with the added variables. 
    The value of the objective is the same since we did not change the logic of the LP   """

