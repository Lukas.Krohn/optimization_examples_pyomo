"""
 Pyomo documention:
 https://pyomo.readthedocs.io/en/stable/
 Pyomo tutorial:
 https://www.osti.gov/servlets/purl/1376827
 Pyomo examples:
 https://github.com/Pyomo/pyomo
"""

from pyomo.environ import *

# Creating a concrete model. (abstract models also possible)
model = ConcreteModel()

# Defining decision variables x and y
# Decision variables x is a non negative integer e.g 0, 1, 2, 3, ...
# Decision variables y is a non negative real e.g 0, 0.1, 1.0, 2.34, ...
model.x = Var(within=NonNegativeIntegers)
model.y = Var(within=NonNegativeReals)

# Defining the constraints NB1, NB2, NB3 and NB6
# Definition of decision variable x takes into account NB4 and NB7
# Definition of decision variable y takes into account NB5
#
# NB1
model.nb1 = Constraint(expr=2*model.x + model.y <= 20)
# NB2
model.nb2 = Constraint(expr=-4*model.x + 5*model.y <= 10)
# NB3
model.nb3 = Constraint(expr=-model.x + 2*model.y >= -2)
# NB6
model.nb6 = Constraint(expr=-model.x + 5*model.y == 15)
# NB6_1 where no solution exist
# model.nb6_1 = Constraint(expr=-model.x + 2*model.y == 12)

# Objective
# sense=maximize defines that the objective has to be maximized
# sense=minimize will minimize the objective. minimize is the default in pyomo
model.objective = Objective(expr=model.x + 2*model.y, sense=maximize)

# To solve the system of equation you need to define a solver in SolverFactory. e.g. cplex
opt = SolverFactory('cbc')
# opt = SolverFactory('cplex')

# .solve function calls the solver and hands over the defined model.
# the solver will solve the model and hands over the results.
# tee=True prints solver information
# logfile saves the solver printout in the a file by the given name e.g. 'log_file'
result_obj = opt.solve(model, tee=True, logfile='log_file')

# prints model
model.pprint()

# print value of decision variables x and y
print('\nDecision variables x: {}'.format(model.x.value))
print('Decision variables y: {}'.format(model.y.value))
# print value of objective
print('Value of objective z: {}'.format(model.objective()))
