"""
 Pyomo documention:
 https://pyomo.readthedocs.io/en/stable/
 Pyomo tutorial:
 https://www.osti.gov/servlets/purl/1376827
 Pyomo examples:
 https://github.com/Pyomo/pyomo
"""

from pyomo.environ import *


""" This linear optimization problem (LP) asks for 
 the most valuable tools by limited weight that can be carried"""

# List describes items, e.g. four tools
A = ['hammer', 'wrench', 'screwdriver', 'towel']

# Dictionary gives each tool a value
v = {'hammer': 8, 'wrench': 3, 'screwdriver': 6, 'towel': 11}

# Dictionary gives each tool a weight
w = {'hammer': 5, 'wrench': 7, 'screwdriver': 4, 'towel': 3}

# Parameter describes the maximum of weight that can be carried
W_max = 14

# Creating a concrete model. (abstract models also possible)
model = ConcreteModel()

# Creating Variables X for every item in the list of A.
# Variables X are binary. So can just be 0 or 1.
# 1 means is taking with you, 0 means is not taken with you.
model.x = Var(A, within=Binary)

# The objective of the LP is to maximize the value of the tools, that are carried with you
# expr describes the expression of the objective.
# Beside sense=maximize there is also sense=minimize.
model.value = Objective(expr=sum(v[i]*model.x[i] for i in A), sense=maximize)

# The constraint of the LP is that the weight of tools carrying with you is limited to W_max
# expr describes the expression of the constraint
model.weight = Constraint(expr=sum(w[i]*model.x[i] for i in A) <= W_max)

# Pyomo just defines the system of equation but can't solve it.
# To solve the system of equation you need to define a solver in SolverFactory. e.g. cplex
opt = SolverFactory('cplex')

# .solve function calls the solver (e.g. cplex) hands over the defined model.
# the solver will solve the model and hands over the results.
# tee=True prints solver information
# logfile saves the solver printout in the a file by the given name e.g. 'log_file'
result_obj = opt.solve(model, tee=True, logfile='log_file')

# prints model
model.pprint()
# model.display()

# Prints each tool with its binary value
for i in model.x:
    binary_value = model.x[i].value
    tool = i
    print(tool, binary_value)

# Prints the value of constraint (weight of tools carrying with you)
print('Total weight of tools', model.weight())

# Prints the objective (value of the tools carrying with you)
print('Total value of tools', model.value())
